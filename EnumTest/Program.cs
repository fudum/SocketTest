﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace EnumTest
{
    class Program
    {
        static void Main(string[] args)
        {



            //自动的断点
            System.Diagnostics.Debugger.Break();

            #region

            //枚举强制类型转换测试
            //Error er = (Error)1; 

            //测试现在执行的代码行和列
            //Test();

            ////测试字符串为空时相加
            //string s_1 = "nidnog";
            //string s_2 = null;
            //string s3 = s_1 + s_2;

            ////test 
            //byte mesid_1 = 0x70;
            //byte mesid_2 = 0x1C;

            //string retval;
            //string val_0 = Convert.ToString(mesid_1, 16);
            //string val_1 = Convert.ToString(mesid_2, 16);
            //retval = val_0.Length == 1 ? "0" + val_0 : val_0;
            //retval += val_1.Length == 1 ? "0" + val_1 : val_1;

            // test stringbuilder
            //StringBuilder sb = new StringBuilder("?");
            //sb.Append("nihao=");
            //string s = null;
            //sb.Append(s);

            ////test  a == 1 ?　1 : 0
            //int i_1 = -1;
            //int i_2 = i_1 == 1 ? 1 : i_1 == 2 ? 2 : i_1 == 3 ? 3 : i_1;

            //// test  ToString()
            //int a = -1;
            //string s = a.ToString();
            //Console.WriteLine((int)Error.error1);
            //Console.ReadLine();

            #endregion
        }

        static void Test()
        {
            //var st = new System.Diagnostics.StackTrace();
            //return st.GetFrame(0).ToString();
            StackTrace st = new StackTrace(new StackFrame(true));
            Console.WriteLine(" Stack trace for current level: {0}", st.ToString());
            StackFrame sf = st.GetFrame(0);
            Console.WriteLine(" File: {0}", sf.GetFileName());
            Console.WriteLine(" Method: {0}", sf.GetMethod().Name);
            Console.WriteLine(" Line Number: {0}", sf.GetFileLineNumber()); Console.WriteLine(" Column Number: {0}", sf.GetFileColumnNumber());
        }

    }
}
