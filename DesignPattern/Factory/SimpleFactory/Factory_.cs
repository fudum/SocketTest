﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DesignPattern.Factory.SimpleFactory
{
    class Factory_
    {
        public static BMW CreateBMW(int type)
        {
            switch (type)
            {
                case 320:
                    return new BMW320();
                case 520:
                    return new BMW520();
                case 620:
                    return new BMW620();
                default:
                    return null;
            }
        }
    }
}
