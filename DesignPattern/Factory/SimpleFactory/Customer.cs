﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DesignPattern.Factory.SimpleFactory
{
    class Customer
    {
        private BMW bmw;

        private void GetBMW(int type)
        {
            bmw =  Factory_.CreateBMW(type);
        }
    }
}
