﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
namespace DesignPattern.Factory.FactoryFunction
{
    class Customer
    {
        private BMW bmw;

        private void GetBMW(int type)
        {
            switch (type)
            {
                case 320:
                    bmw = new FactoryBMW320().CreateBMW();
                    break;
                case 520:
                    bmw = new FactoryBMW520().CreateBMW();
                    break;
            }
        }

        /// <summary>
        /// 反射机制
        /// </summary>
        /// <param name="name"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        //private BMW GetBMWByReflection(string name, int type)
        //{
        //    System.Reflection.Assembly a;
        //    a = System.Reflection.Assembly.LoadFrom(name + type);
        //    Object ob = a.CreateInstance(name + type);
            
            
        //}

        
    }
}
