﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DesignPattern.Factory.FactoryFunction
{
    class FactoryBMW320 : FactoryBMW
    {
        public BMW CreateBMW()
        {
            return new BMW320();
        }
    }
}
