﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DesignPattern.Factory.FactoryFunction
{
    interface FactoryBMW
    {
        BMW CreateBMW();
    }
}
