﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DesignPattern.Factory.AbstractFactory
{
    interface FactoryBMW
    {
        BMW CreateBMW();
        Aircondition CreateAircondition();
    }
}
