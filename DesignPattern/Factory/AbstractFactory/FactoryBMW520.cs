﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DesignPattern.Factory.AbstractFactory
{
    class FactoryBMW520 : FactoryBMW
    {
        public BMW CreateBMW()
        {
            return new BMW520();
        }

        public Aircondition CreateAircondition()
        {
            return new AirconditionBMW520();
        }
    }
}
