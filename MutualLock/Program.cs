﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MutualLockTest.MonitorTest;
using System.Threading;

namespace MutualLockTest
{
    class Program
    {
        static void Main(string[] args)
        {
            MonitorSample ss = new MonitorSample();
            Thread thdProduce = new Thread(new ThreadStart(ss.Produce));
            Thread thdConsume = new Thread(new ThreadStart(ss.Consume));
            Console.WriteLine("开始启动线程，输入回车终止生产者和消费者的工作……\r\n");
            thdProduce.Start();
            Thread.Sleep(2000);
            thdConsume.Start();
            Console.ReadLine();
            ss.StopThread();
            Thread.Sleep(1000);
            while (thdProduce.ThreadState != ThreadState.Stopped)
            {
                ss.StopThread();
                thdProduce.Join(100);
            }
            while (thdConsume.ThreadState != ThreadState.Stopped)
            {
                ss.StopThread();
                thdConsume.Join(100);
            }
            Console.WriteLine("***********************************");
            Console.ReadLine();
        }
    }
}
