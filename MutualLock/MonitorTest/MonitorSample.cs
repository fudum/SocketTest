﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Threading;

namespace MutualLockTest.MonitorTest
{
    class MonitorSample
    {
        private ArrayList candyBox = new ArrayList(1);
        private volatile bool shouldStop = false;   //用于控制线程正常结束的标志

        public void StopThread()
        {
            shouldStop = true;
            Monitor.Enter(candyBox);
            try
            {
                Monitor.PulseAll(candyBox);
            }
            catch (System.Exception ex)
            {
            	
            }
            finally
            {
                Monitor.Exit(candyBox);
            }
        }

        public void Produce()
        {
            while (!shouldStop)
            {
                Monitor.Enter(candyBox);
                try
                {
                    if (candyBox.Count == 0)
                    {
                        candyBox.Add("a candy");
                        Console.WriteLine("生产者： 有糖吃了");
                        Monitor.Pulse(candyBox);
                        Console.WriteLine("生产者：赶快来吃");
                        Monitor.Wait(candyBox);
                    }
                    else
                    {
                        Console.WriteLine("生产者：糖罐是满的");
                        Monitor.Pulse(candyBox);
                        Monitor.Wait(candyBox);
                    }
                }
                catch (System.Exception ex)
                {
                	
                }
                finally
                {
                    Monitor.Exit(candyBox);
                }
                Thread.Sleep(10);
            }
            Console.WriteLine("生产者：下班了");
        }

        public void Consume()
        {
            while (!shouldStop || candyBox.Count > 0)
            {
                Monitor.Enter(candyBox);
                try
                {
                    if (candyBox.Count == 1)
                    {
                        candyBox.RemoveAt(0);
                        if (!shouldStop)
                        {
                            Console.WriteLine("消费者：糖已经吃完了");
                        }
                        else
                        {
                            Console.WriteLine("消费者：还有糖没有吃，马上就完了");
                        }
                        Monitor.Pulse(candyBox);
                        Console.WriteLine("消费者：赶快生产");
                        Monitor.Wait(candyBox);
                    }
                    else
                    {
                        Console.WriteLine("消费者：糖罐是空的");
                        Monitor.Pulse(candyBox);
                        Monitor.Wait(candyBox);
                    }
                }
                catch (System.Exception ex)
                {
                	
                }
                finally
                {
                    Monitor.Exit(candyBox);
                }
            }
            Console.WriteLine("消费者：都吃光啦，下次再吃");
        }

    }
}
