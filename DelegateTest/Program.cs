﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DelegateTest
{
    delegate double MathAction(double num);

    class Program
    {

        static double Double(double input)
        {
            return input * 2;
        }

        static void Main(string[] args)
        {

            DelTest1.AllFunction();
            System.Diagnostics.Debugger.Break();
            // Instantiate delegate with named method:
            MathAction ma = Double;

            // Invoke delegate ma:
            double multByTwo = ma(4.5);
            Console.WriteLine(multByTwo);

            // Instantiate delegate with anonymous method:
            MathAction ma2 = delegate(double input)
            {
                return input * input;
            };

            double square = ma2(5);
            Console.WriteLine(square);

            // Instantiate delegate with lambda expression
            MathAction ma3 = s => s * s * s;
            double cube = ma3(4.375);

            Console.WriteLine(cube);
        }

    }
}
