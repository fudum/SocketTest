﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace ValueAndRefTest
{
    class Program
    {
        static void Main(string[] args)
        {
            //array test
            ArrayList arrayList = new ArrayList();
            ArrayList arrayList_1 = new ArrayList();            
            for (int i = 0; i < 10; i++ )
            {
                User user = new User();
                user.age = i;
                arrayList.Add(user);
            }
            for (int i = 0; i < arrayList.Count; i++ )
            {
                Console.WriteLine(((User)arrayList[i]).age);
            }

            //struct test
            StructUser structUser;
            for (int i = 0; i < 10; i ++ )
            {                
                structUser.age = i;
                arrayList_1.Add(structUser);                
            }
            for (int i = 0; i < arrayList_1.Count; i++ )
            {
                Console.WriteLine(((StructUser)arrayList_1[i]).age);                
            }
            Console.ReadLine();
        }
    }
}
