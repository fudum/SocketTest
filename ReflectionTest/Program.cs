﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;

namespace ReflectionTest
{
    class Program
    {
        static void Main(string[] args)
        {
            Type t = typeof(People);
            Console.WriteLine("----------------Method方法------------------");
            MethodInfo[] methods = t.GetMethods();
            foreach (MethodInfo method in methods)
            {
                Console.WriteLine("Method:" + method);
            }
            Console.WriteLine();

            Console.WriteLine("---------------Field字段-------------------");
            FieldInfo[] fields = t.GetFields(BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static);
            foreach (FieldInfo field in fields)
            {
                Console.WriteLine("Field:" + field); 
            }
            Console.WriteLine();

            Console.WriteLine("--------------Member成员--------------------"); 
            MemberInfo[] members = t.GetMembers();
            foreach (MemberInfo member in members)
            {
                Console.WriteLine("Member:" + member); 
            }
            Console.WriteLine();

            Console.WriteLine("--------------Property属性--------------------");
            PropertyInfo[] properties = t.GetProperties();
            foreach (PropertyInfo property in properties)
            {
                Console.WriteLine("Property:" + property); 
            }
            Console.WriteLine();

            Console.WriteLine("--------------Constructor构造方法-----------------");
            ConstructorInfo[] constructors = t.GetConstructors(BindingFlags.NonPublic | BindingFlags.Instance);
            foreach (ConstructorInfo constructor in constructors)
            {
                Console.WriteLine("Constructor:" + constructor);
            }
            Console.WriteLine();

            Console.ReadLine();
        }
    }
}
