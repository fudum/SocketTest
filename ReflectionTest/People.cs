﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ReflectionTest
{
    class People
    {
        private static string name = "yufengjian";    //字段 
        private string sex = "man";  //字段 

        public string Sex     //属性 
        {
            get { return sex; }
            set { sex = value; }
        }

        public static string Name    //属性 
        {
            get { return People.name; }
            set { People.name = value; }
        }

       

        public static string GetName()    // 函数 
        {
            if (string.IsNullOrEmpty(name))
            {
                name = "my name"; 
            }
            return name; 
        }


    }
}
