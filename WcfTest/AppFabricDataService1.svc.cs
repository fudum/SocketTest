﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Data.Services;
using System.Data.Services.Common;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Web;
using System.Web.Routing;

namespace WcfTest
{
    // For help with this template see http://go.microsoft.com/?linkid=9737621

    // TODO: Eliminates the need to use the .SVC extension in your URI
    // For example http://localhost/AppFabricDataService1

    // TODO: This requires <serviceHostingEnvironment aspNetCompatibilityEnabled="true" /> in your web.config
    // See http://msdn.microsoft.com/en-us/library/ms731336.aspx for more information
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]

    public class AppFabricDataService1 : DataService</* TODO: put your data source class name here */>
    {
        // This method is called only once to initialize service-wide policies.
        public static void InitializeService(DataServiceConfiguration config)
        {            
            // TODO: set rules to indicate which entity sets and service operations are visible, updatable, etc.
            // Examples:
            // config.SetEntitySetAccessRule("MyEntityset", EntitySetRights.AllRead);
            // config.SetServiceOperationAccessRule("MyServiceOperation", ServiceOperationRights.All);
            config.DataServiceBehavior.MaxProtocolVersion = DataServiceProtocolVersion.V2;

            // TODO: Set your route name
            RouteTable.Routes.Add(
                new ServiceRoute("AppFabricDataService1", new DataServiceHostFactory(), typeof(AppFabricDataService1)));

        }

        // TODO: Modify web.config
        // To see these events in Windows Server AppFabric you must enable End-To-End Monitoring in the tracking 
        // See http://msdn.microsoft.com/en-us/library/ee677196.aspx

        #region Sample Config with End-To-End Monitoring enabled
        /* 
            <system.serviceModel>
	            <serviceHostingEnvironment aspNetCompatibilityEnabled="true" />
	            <diagnostics etwProviderId="830b12d1-bb5b-4887-aa3f-ab508fd4c8ba">
		            <endToEndTracing propagateActivity="true" messageFlowTracing="true" />
	            </diagnostics>
	            <behaviors>
		            <serviceBehaviors>
			            <behavior name="">
				            <etwTracking profileName="EndToEndMonitoring Tracking Profile" />
			            </behavior>
		            </serviceBehaviors>
	            </behaviors>
            </system.serviceModel>
            <system.webServer>
                <modules runAllManagedModulesForAllRequests="true">
                    <remove name="UrlRoutingModule"/>
                    <add name="UrlRoutingModule" type="System.Web.Routing.UrlRoutingModule, System.Web, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" />
                </modules>
                <handlers>
                    <add
                    name="UrlRoutingHandler"
                    preCondition="integratedMode"
                    verb="*" path="UrlRouting.axd"
                    type="System.Web.HttpForbiddenHandler, System.Web, 
                        Version=2.0.0.0, Culture=neutral, 
                        PublicKeyToken=b03f5f7f11d50a3a"/>
                </handlers>
            </system.webServer>          
         */
        #endregion

        static AppFabricDataService1EventProvider eventProvider = new AppFabricDataService1EventProvider();

        protected override void HandleException(HandleExceptionArgs args)
        {
            eventProvider.WriteErrorEvent(
                this.GetType().Name,
                "Exception {0} status code {1}",
                args.Exception.Message,
                args.ResponseStatusCode);
        }

        protected override void OnStartProcessingRequest(ProcessRequestArgs args)
        {
            eventProvider.WriteInformationEvent(
                this.GetType().Name,
                "Processing HTTP {0} request for URI {1}",
                args.OperationContext.RequestMethod,
                args.RequestUri);
        }
    }
}
