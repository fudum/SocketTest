﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Threading;
using System.IO;
using Log;

namespace WatchDog
{
    class HttpServerTest
    {
        public HttpServerTest()
        {
            ViewHttpServer();
        }

        private void ViewHttpServer()
        {
            bool flag = true;
            string retval;
            while (flag)
            {
                MyXml myxml = new MyXml();
                string url = "http://192.168.1.13:85/Default.aspx";
                int timesec = 30000;
                try
                {
                    url = myxml.XmlNodeRead("propath_1");
                    timesec = int.Parse(myxml.XmlNodeRead("prosec"));
                }
                catch (System.Exception ex)
                {
                    Logs.WriteLog(LogFile.IISError, ex.ToString());
                }
                try
                {
                    HttpWebRequest webReq = (HttpWebRequest)HttpWebRequest.Create(url);
                    webReq.Method = "GET";
                    HttpWebResponse webRep = webReq.GetResponse() as HttpWebResponse;   //返回来自 Internet 资源的响应。
                    retval = webRep.StatusDescription;
                    Thread.Sleep(10);
                    webRep.Close();
                }
                catch (System.Exception ex)
                {
                    Logs.WriteLog(LogFile.IISError, ex.ToString());
                }
                Thread.Sleep(timesec);
            }

        }
    }
}
