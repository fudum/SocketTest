﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.IO;
using System.Threading;
using Log;

namespace WatchDog
{
    class MyXml
    {
        public void CreateXmlconf(string xmlstr)
        {
            try
            {
                if (!File.Exists(Environment.CurrentDirectory + "\\Configure"))
                {
                    System.IO.Directory.CreateDirectory(Environment.CurrentDirectory + "\\Configure");
                }

                if (File.Exists(Environment.CurrentDirectory + "\\Configure\\" + xmlstr))
                {
                    return;
                }
                else
                {
                    using (FileStream fs = File.Create(Environment.CurrentDirectory + "\\Configure\\" + xmlstr))
                    {
                        Thread.Sleep(1000);
                    }                    
                    XmlDocument Xmldoc = new XmlDocument();
                    XmlDeclaration xmldec;
                    xmldec = Xmldoc.CreateXmlDeclaration("1.0", "GBK", null);
                    Xmldoc.AppendChild(xmldec);
                    XmlNode xmlnode;
                    xmlnode = Xmldoc.CreateElement("pro");
                    XmlElement xmlele;
                    xmlele = Xmldoc.CreateElement("pronum");
                    xmlele.InnerText = "1";
                    xmlnode.AppendChild(xmlele);
                    xmlele = Xmldoc.CreateElement("propath_1");
                    xmlele.InnerText = "http://192.168.1.13:85/Default.aspx";
                    xmlnode.AppendChild(xmlele);
                    xmlele = Xmldoc.CreateElement("prosec");
                    xmlele.InnerText = "30000";
                    xmlnode.AppendChild(xmlele);
                    Xmldoc.AppendChild(xmlnode);
                    Xmldoc.Save(Environment.CurrentDirectory + "\\Configure\\" + xmlstr);
                }

            }
            catch (Exception e)
            {
                Logs.WriteLog(LogFile.IISError, e.ToString());
            }
        }
        public string XmlNodeRead(string Xname)
        {
            string tempstr = null;
            try
            {
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load(Environment.CurrentDirectory + "\\Configure\\iisconf.xml");
                XmlNodeList xmllist = xmlDoc.SelectSingleNode("pro").ChildNodes;
                foreach (XmlNode xmlNo in xmllist)
                {
                    XmlElement xe = (XmlElement)xmlNo;
                    if (xe.Name == Xname)
                    {
                        tempstr = xe.InnerText;
                        return tempstr;
                    }
                }
            }
            catch (Exception e)
            {
                //读取错误
                if (!Directory.Exists(System.Environment.CurrentDirectory + "\\Errlog") && e.Message != null)
                {
                    Directory.CreateDirectory(System.Environment.CurrentDirectory + "\\Errlog");
                }
                string temp = null;
                if (File.Exists(System.Environment.CurrentDirectory + "\\Errlog\\" + DateTime.Now.ToString("yyyy-MM-dd") + ".txt"))
                {
                    temp = File.ReadAllText(System.Environment.CurrentDirectory + "\\Errlog\\" + DateTime.Now.ToString("yyyy-MM-dd") + ".txt");
                }
                temp += "[" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "]" + e.Message;
                File.WriteAllText(System.Environment.CurrentDirectory + "\\Errlog\\" + DateTime.Now.ToString("yyyy-MM-dd") + ".txt", temp + "\r\n");
            }
            return tempstr;
        }
    }
}
