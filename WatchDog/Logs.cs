﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Log
{

    /// <summary>
    ///Logs 的摘要说明
    /// </summary>
    public class Logs
    {
        private static LogManager logManager;
        static Logs()
        {
            logManager = new LogManager();
        }

        public static void WriteLog(LogFile logFile, string msg)
        {
            logManager.WriteLog(logFile, msg);
        }

        public static void WriteLog(string logFile, string msg)
        {
            logManager.WriteLog(logFile, msg);
        }

    }
}