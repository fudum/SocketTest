﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;
using System.Threading;

namespace HttpPostTest
{
    class Program
    {
        static void Main(string[] args)
        {
            int i = 0;
            while (true)
            {
                i++;
                //string Url = "http://61.183.9.107:3331/LocationUnifiedReceiver.aspx";
                string Url = "http://61.183.9.107:81/vehiclegps/ConcreteLatestVehicleInfo.aspx";
                CookieContainer cookie = new CookieContainer();
                string postDataStr = "Sim=013635726274";
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(Url);
                request.Method = "POST";
                request.ContentType = "application/x-www-form-urlencoded";
                request.CookieContainer = cookie;
                byte[] buf = Encoding.UTF8.GetBytes(postDataStr);
                using (Stream stream = request.GetRequestStream())
                {
                    stream.Write(buf, 0, buf.Length);
                }
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                string retString = response.StatusDescription;
                i++;
                if (i == 100)
                {
                    System.Diagnostics.Debugger.Break();
                }
            }
        }

        private void SendTwo(string Url, string postDataStr)
        {
            CookieContainer cookie = new CookieContainer();
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(Url);
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";
            request.ContentLength = Encoding.UTF8.GetByteCount(postDataStr);
            request.CookieContainer = cookie;
            Stream myRequestStream = request.GetRequestStream();
            StreamWriter myStreamWriter = new StreamWriter(myRequestStream, Encoding.GetEncoding("gb2312"));
            myStreamWriter.Write(postDataStr);
            //myStreamWriter.Close();

            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            response.Cookies = cookie.GetCookies(response.ResponseUri);
            Stream myResponseStream = response.GetResponseStream();
            StreamReader myStreamReader = new StreamReader(myResponseStream, Encoding.GetEncoding("utf-8"));
            string retString = myStreamReader.ReadToEnd();
            //myStreamReader.Close();
            //myResponseStream.Close();
        }
    
    }
}
