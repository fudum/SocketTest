﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Net;

namespace MultiThreadClient
{
    class Program
    {
        static void Main(string[] args)
        {
            Socket client;
            byte[] buf = new byte[1024];
            string input;
            IPAddress local = IPAddress.Parse("59.69.105.58");
            IPEndPoint iep = new IPEndPoint(local, 13000);
            try
            {
                client = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.IP);
                client.Connect(iep);
                bool blockingstate = client.Blocking;
            }
            catch (SocketException)
            {
                Console.WriteLine("无法连接到服务器.....");
                return;
            }
            finally
            {

            }
            while (true)
            {
                try
                {
                    input = Console.ReadLine();
                    if (input == "exit")
                    {
                        break;
                    }

                    client.Blocking = true;
                    client.Send(Encoding.UTF8.GetBytes(input));
                    int rec = client.Receive(buf);
                    Console.WriteLine(Encoding.UTF8.GetString(buf, 0, rec));
                    //------问题在这 cdlient.Close();  //System.ObjectDisposedException: 无法访问已释放的对象
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                }

            }

        }
    }
}
