﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Net;
using System.Threading;

namespace ThreadTest
{
    class ServerThread
    {
        private Socket server;
         public ServerThread()
        {
            IPAddress local = IPAddress.Parse("59.69.105.58");
            IPEndPoint iep = new IPEndPoint(local, 13000);
            server = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.IP);
            server.Bind(iep);
            server.Listen(20);
            Console.WriteLine("等待客户机连接.......");
            while (true)
            {
                Socket client = server.Accept();
                Clientthread ct = new Clientthread(client);
                Thread newthread = new Thread(new ThreadStart(ct.ClientService));
                newthread.Start();

              }
        }
    }
}
