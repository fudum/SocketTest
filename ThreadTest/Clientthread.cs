﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;

namespace ThreadTest
{
    class Clientthread
    {
        public static int connections = 0;//表示连接的客户端数量
        public Socket service;
        int i;
        public Clientthread(Socket clientsocket)
        {
            this.service = clientsocket;
        }

        public void ClientService()
        {
            string data = null;
            byte[] bytes = new byte[1024];
            if (service != null)
            {
                connections++;
            }
            Console.WriteLine("第{0}个客户端建立", connections);
            while ((i = service.Receive(bytes)) != 0)
            {
                data = Encoding.UTF8.GetString(bytes, 0, i);
                data = data.ToUpper();
                byte[] msg = Encoding.UTF8.GetBytes(data);
                service.Send(msg);
                Console.WriteLine("发送数据为：{0}", data);
            }
            service.Close();
            connections--;
            Console.WriteLine("客户端关闭连接:{0}个连接数", connections);
        }
    }
}
