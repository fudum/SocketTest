﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CLRvirTest
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, Inherited = true)]
    class TastyAttribute :Attribute
    {
    }

    [Tasty][Serializable]
    internal class BaseType
    {
        [Tasty]
        protected virtual void DoSomething() { }
    }

    internal class DerivedType : BaseType
    {
        //要扩展或修改继承的方法、属性、索引器或事件的抽象实现或虚实现，必须使用 override 修饰符。
        protected override void DoSomething() { }
    }
}
