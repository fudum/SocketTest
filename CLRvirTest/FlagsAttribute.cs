﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CLRvirTest
{
    //类名有一个attribute的后缀；这是为了保持与标准的相容性
    [AttributeUsage(AttributeTargets.Enum,Inherited = false)]
    class FlagsAttribute : System.Attribute
    {
        public FlagsAttribute()
        {

        }
    }
}
