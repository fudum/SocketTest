﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;
using System.Security;
using System.Runtime.InteropServices;

namespace CLRvirTest
{
    class Program
    {
        static void Main(string[] args)
        {
            int i = StaticTest.a.Count;
            int num = 515;
            byte sssss = (byte)num;
            Console.WriteLine(sssss);
            System.Diagnostics.Debugger.Break();
            //Double d;
            //d = Char.GetNumericValue('\u0033');
            //Console.WriteLine(d.ToString());

            //d = Char.GetNumericValue('\u00bc');
            //Console.WriteLine(d);

            //d = Char.GetNumericValue('A');
            //Console.WriteLine(d.ToString());

            //String s1 = "Hello";
            //String s2 = "Hello";
            //Console.WriteLine(Object.ReferenceEquals(s1, s2));

            //s1 = String.Intern(s1);
            //s1 = String.Intern(s2);
            //Console.WriteLine(Object.ReferenceEquals(s1, s2));

            //String s = "a\u0304\u0308bc\u0327";
            //Console.WriteLine(SubStringByTextElements(s));
            //StringBuilder sb = new StringBuilder();
            //Object o = new object();
            //Int32 x = Int32.Parse(" 123", NumberStyles.AllowLeadingWhite, null);


            //string 和 byte[] 之间的转换
            //String s = "Hi there.";
            //Encoding encoding = Encoding.UTF8;
            //byte[] encodedBytes = encoding.GetBytes(s);
            //Console.WriteLine("Encoding Bytes:" + BitConverter.ToString(encodedBytes));
            //string deencoding = encoding.GetString(encodedBytes);
            //Console.WriteLine("Decoded string :" + deencoding);

            //加密字符的使用
            //using (SecureString ss = new SecureString())
            //{
            //    Console.WriteLine("please inter password:");
            //    while (true)
            //    {
            //        ConsoleKeyInfo cki = Console.ReadKey(true);
            //        if (cki.Key == ConsoleKey.Enter)
            //        {
            //            break;
            //        }
            //        ss.AppendChar(cki.KeyChar);
            //        Console.WriteLine("*");
            //    }
            //    Console.WriteLine();
            //    DisplaySecureString(ss);
            //}

            ////System.Decimal
            //Array a;
            //a = new String[0];
            //Console.WriteLine(a.GetType());

            //a = Array.CreateInstance(typeof(String), new Int32[] { 0 }, new Int32[] { 0 });

            //可空值类型
            //Int32? a = 3;
            //Int32? b = null;
            //Int32 c = (Int32)a;
            //Int32? a = 5;
            //Int32? b = null;
            //a++;
            //b = -b;
            //a = a + 3;
            //b = b + 3;

            //异常处理机制
            Boolean f = "Jeff".Substring(1, 1).ToUpper().EndsWith("E");


            

        }

        //public unsafe static void DisplaySecureString(SecureString ss)
        //{
        //    Char* pc = null;
        //    try
        //    {
        //        pc = (Char*)Marshal.SecureStringToCoTaskMemUnicode(ss);
        //        for (Int32 index = 0; pc[index] != 0; index++ )
        //        {
        //            Console.Write(pc[index]);
        //        }
        //    }
        //    catch (System.Exception ex)
        //    {
            	
        //    }
        //    finally
        //    {
        //        if (pc != null)
        //        {
        //            Marshal.ZeroFreeCoTaskMemUnicode((IntPtr)pc);
        //        }
        //    }
        //}

        public static string SubStringByTextElements(string s)
        {
            String output = String.Empty;
            StringInfo si = new StringInfo(s);
            for (Int32 element = 0; element < si.LengthInTextElements;element++)
            {
                output += String.Format("Text element {0} is '{1}'{2}",element,si.SubstringByTextElements(element,1),Environment.NewLine);
            }
            return output;

        }

        static Int32 NumTimesWordAppearsEquals(string word, string[] wordList)
        {
            Int32 count = 0;
            for (Int32 wordNum = 0; wordNum < wordList.Length; wordNum ++ )
            {
                if (word.Equals(wordList[wordNum],StringComparison.Ordinal))
                {
                    count++;
                }
            }
            return count;
        }

        static Int32 NumTimesWordAppearsIntern(string word, string[] wordList)
        {
            word = String.Intern(word);
            Int32 count = 0;
            for (Int32 wordNum = 0; wordNum < wordList.Length; wordNum++)
            {
                if (Object.ReferenceEquals(word,wordList[wordNum]))
                {
                    count++;
                }
            }
            return count;
        }
    }
}
