﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;

namespace ServerPressTest
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            StartBtn.Enabled = true;
            StopBtn.Enabled = false;
        }

        //开始发送数据
        private void StartBtn_Click(object sender, EventArgs e)
        {
            StartBtn.Enabled = false;
            StopBtn.Enabled = true;
            m_nThreadNum = Convert.ToInt32(SendThreadNumTxt.Text.Trim());
            int nSendTimes = Convert.ToInt32(SendTimesTxt.Text.Trim());
            String ip = ServerIPTxt.Text.Trim();
            int port = Convert.ToInt32(ServerPortTxt.Text.Trim());
            int sendInterval = Convert.ToInt32(SendIntervalTxt.Text.Trim());
            //默认是文本形式的数据
            int dataType = 0;
            if (BinaryBtn.Checked)
            {
                //二进制形式
                dataType = 1;
            }
            byte[] sendData = DataExchange(dataType,SendDataTxt.Text.Trim());
            m_SendThread = new Thread[m_nThreadNum];
            m_SendClient = new Client[m_nThreadNum];
            for (int i = 0; i < m_nThreadNum; ++i )
            {
                m_SendClient[i] = new Client(ip,port,nSendTimes,sendInterval,sendData);
                m_SendThread[i] = new Thread(new ThreadStart(m_SendClient[i].run));
                m_SendThread[i].Start();
            }
        }

        //停止发送
        private void StopBtn_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < m_nThreadNum; ++i )
            {
                m_SendClient[i].Stop();
                m_SendThread[i].Join(30000);
                m_SendThread[i].Abort();
                m_SendClient[i] = null;
                m_SendThread[i] = null;
            }
            StartBtn.Enabled = true;
            StopBtn.Enabled = false;
        }

        //数据转换
        private byte[] DataExchange(int dataType, String data)
        {
            byte[] sendData = null;
            if (dataType == 1)
            {
                data = data.ToUpper();
                data = data.Replace(" ", "");
                if (data.Length % 2 != 0)
                {
                    data = "0" + data;
                }
                int len = data.Length / 2;
                sendData = new byte[len];
                for (int i = 0; i < len; ++i )
                {
                    sendData[i] = 0;
                    sendData[i] |= (byte)(Hex2Num(data[2 * i]) << 4);
                    sendData[i] |= Hex2Num(data[2 * i + 1]);
                }
            }
            else
            {
                //文本形式的
                sendData = Encoding.ASCII.GetBytes(data);
            }
            return sendData;
        }

        //16进制转10进制
        private byte Hex2Num(char ch)
        {
            if (ch >= '0' && ch <= '9')
            {
                return (byte)(ch - '0');
            }
            else if (ch >= 'A' && ch <= 'F')
            {
                return (byte)(ch - 'A' + 10);
            }
            else if (ch >= 'a' && ch <= 'f')
            {
                return (byte)(ch - 'a' + 10);
            }
            return 0;
        }
        private Thread[] m_SendThread;          //发送线程数组
        private Client[] m_SendClient;          //发送客户端数组
        private int m_nThreadNum = 1;           //发送线程个数

        private void BinaryBtn_Click(object sender, EventArgs e)
        {
            SendDataTxt.Text = "7e 80 01 00 05 01 58 97 84 84 51 00 02 4c fb 00 02 00 ac 7e 7e 02 00 00 26 01 58 97 84 84 51 4c fc 00 00 00 00 00 00 00 c2 01 d3 03 ee 06 cc 85 b0 00 00 00 00 00 01 13 10 09 15 12 50 01 04 00 00 14 fb 03 02 00 00 be 7e";
        }

        private void TxtBtn_Click(object sender, EventArgs e)
        {
            SendDataTxt.Text = "*HQ20113477328651,AH&A1259492813259111255709460000091013&B0200000000&C002794?7&F0000#";
        }

              
    }
}
