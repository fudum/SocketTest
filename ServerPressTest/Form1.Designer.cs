﻿namespace ServerPressTest
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.label1 = new System.Windows.Forms.Label();
            this.ServerIPTxt = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.ServerPortTxt = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this.SendIntervalTxt = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.SendTimesTxt = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.SendThreadNumTxt = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.TxtBtn = new System.Windows.Forms.RadioButton();
            this.BinaryBtn = new System.Windows.Forms.RadioButton();
            this.label6 = new System.Windows.Forms.Label();
            this.SendDataTxt = new System.Windows.Forms.TextBox();
            this.StartBtn = new System.Windows.Forms.Button();
            this.StopBtn = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(89, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "服务器IP地址：";
            // 
            // ServerIPTxt
            // 
            this.ServerIPTxt.Location = new System.Drawing.Point(99, 10);
            this.ServerIPTxt.Name = "ServerIPTxt";
            this.ServerIPTxt.Size = new System.Drawing.Size(155, 21);
            this.ServerIPTxt.TabIndex = 1;
            this.ServerIPTxt.Text = "59.69.105.58";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 57);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(89, 12);
            this.label2.TabIndex = 2;
            this.label2.Text = "服务器端口号：";
            // 
            // ServerPortTxt
            // 
            this.ServerPortTxt.Location = new System.Drawing.Point(99, 52);
            this.ServerPortTxt.Name = "ServerPortTxt";
            this.ServerPortTxt.Size = new System.Drawing.Size(155, 21);
            this.ServerPortTxt.TabIndex = 3;
            this.ServerPortTxt.Text = "9994";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.SendIntervalTxt);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.SendTimesTxt);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.SendThreadNumTxt);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Location = new System.Drawing.Point(288, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(181, 97);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "测试选项";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(157, 75);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(17, 12);
            this.label8.TabIndex = 13;
            this.label8.Text = "ms";
            // 
            // SendIntervalTxt
            // 
            this.SendIntervalTxt.Location = new System.Drawing.Point(84, 70);
            this.SendIntervalTxt.Name = "SendIntervalTxt";
            this.SendIntervalTxt.Size = new System.Drawing.Size(67, 21);
            this.SendIntervalTxt.TabIndex = 5;
            this.SendIntervalTxt.Text = "1";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(18, 75);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(65, 12);
            this.label7.TabIndex = 4;
            this.label7.Text = "发送间隔：";
            // 
            // SendTimesTxt
            // 
            this.SendTimesTxt.Location = new System.Drawing.Point(84, 47);
            this.SendTimesTxt.Name = "SendTimesTxt";
            this.SendTimesTxt.Size = new System.Drawing.Size(67, 21);
            this.SendTimesTxt.TabIndex = 3;
            this.SendTimesTxt.Text = "1";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(19, 51);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(59, 12);
            this.label4.TabIndex = 2;
            this.label4.Text = "发送次数:";
            // 
            // SendThreadNumTxt
            // 
            this.SendThreadNumTxt.Location = new System.Drawing.Point(84, 19);
            this.SendThreadNumTxt.Name = "SendThreadNumTxt";
            this.SendThreadNumTxt.Size = new System.Drawing.Size(67, 21);
            this.SendThreadNumTxt.TabIndex = 1;
            this.SendThreadNumTxt.Text = "1";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(71, 12);
            this.label3.TabIndex = 0;
            this.label3.Text = "测试线程数:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(15, 83);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(101, 12);
            this.label5.TabIndex = 6;
            this.label5.Text = "测试数据据类型：";
            // 
            // TxtBtn
            // 
            this.TxtBtn.AutoSize = true;
            this.TxtBtn.Location = new System.Drawing.Point(121, 83);
            this.TxtBtn.Name = "TxtBtn";
            this.TxtBtn.Size = new System.Drawing.Size(71, 16);
            this.TxtBtn.TabIndex = 7;
            this.TxtBtn.Text = "文本数据";
            this.TxtBtn.UseVisualStyleBackColor = true;
            this.TxtBtn.Click += new System.EventHandler(this.TxtBtn_Click);
            // 
            // BinaryBtn
            // 
            this.BinaryBtn.AutoSize = true;
            this.BinaryBtn.Checked = true;
            this.BinaryBtn.Location = new System.Drawing.Point(198, 82);
            this.BinaryBtn.Name = "BinaryBtn";
            this.BinaryBtn.Size = new System.Drawing.Size(83, 16);
            this.BinaryBtn.TabIndex = 8;
            this.BinaryBtn.TabStop = true;
            this.BinaryBtn.Text = "二进制数据";
            this.BinaryBtn.UseVisualStyleBackColor = true;
            this.BinaryBtn.Click += new System.EventHandler(this.BinaryBtn_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 112);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(65, 12);
            this.label6.TabIndex = 9;
            this.label6.Text = "测试数据：";
            // 
            // SendDataTxt
            // 
            this.SendDataTxt.Location = new System.Drawing.Point(12, 128);
            this.SendDataTxt.Multiline = true;
            this.SendDataTxt.Name = "SendDataTxt";
            this.SendDataTxt.Size = new System.Drawing.Size(457, 128);
            this.SendDataTxt.TabIndex = 10;
            this.SendDataTxt.Text = resources.GetString("SendDataTxt.Text");
            // 
            // StartBtn
            // 
            this.StartBtn.Location = new System.Drawing.Point(74, 265);
            this.StartBtn.Name = "StartBtn";
            this.StartBtn.Size = new System.Drawing.Size(75, 23);
            this.StartBtn.TabIndex = 11;
            this.StartBtn.Text = "开始";
            this.StartBtn.UseVisualStyleBackColor = true;
            this.StartBtn.Click += new System.EventHandler(this.StartBtn_Click);
            // 
            // StopBtn
            // 
            this.StopBtn.Location = new System.Drawing.Point(288, 265);
            this.StopBtn.Name = "StopBtn";
            this.StopBtn.Size = new System.Drawing.Size(75, 23);
            this.StopBtn.TabIndex = 12;
            this.StopBtn.Text = "停止";
            this.StopBtn.UseVisualStyleBackColor = true;
            this.StopBtn.Click += new System.EventHandler(this.StopBtn_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(481, 300);
            this.Controls.Add(this.StopBtn);
            this.Controls.Add(this.StartBtn);
            this.Controls.Add(this.SendDataTxt);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.BinaryBtn);
            this.Controls.Add(this.TxtBtn);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.ServerPortTxt);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.ServerIPTxt);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "网络服务器压力测试";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox ServerIPTxt;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox ServerPortTxt;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox SendThreadNumTxt;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox SendTimesTxt;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.RadioButton TxtBtn;
        private System.Windows.Forms.RadioButton BinaryBtn;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox SendDataTxt;
        private System.Windows.Forms.Button StartBtn;
        private System.Windows.Forms.Button StopBtn;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox SendIntervalTxt;
        private System.Windows.Forms.Label label8;
    }
}

