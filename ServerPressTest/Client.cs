﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace ServerPressTest
{
    class Client
    {
        public Client(String ip, int port, int sendTimes, int sendInterval, byte[] sendData)
        {
            m_IPAddr = ip;
            m_nPort = port;
            m_SendData = sendData;
            m_nSendTimes = sendTimes;
            m_nSendInterval = sendInterval;
            m_nRunSign = 0;
            newclient = null;
        }

        public void run()
        {
            int sign = m_nSendTimes;
            newclient = null;
            IPEndPoint ie = null;
            m_nRunSign = 1;
            int nConnStatus = 0;
            
            Thread currentThread = Thread.CurrentThread;    //得到当前线程
            Thread recvThread = null;
            while (m_nRunSign == 1 && (sign < 0 || m_nSendTimes >= 0))
            {
                if (nConnStatus == 0)
                {
                    try
                    {
                        ie = new IPEndPoint(IPAddress.Parse(m_IPAddr), m_nPort);//服务器的IP和端口
                        newclient = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                        newclient.Connect(ie);
                        nConnStatus = 1;
                        recvThread = new Thread(new ThreadStart(this.Recv));
                        recvThread.Start();
                    }
                    catch (Exception e)
                    {
                        Thread.Sleep(m_nSendInterval);
                        nConnStatus = 0;
                        continue;
                    }
                }
                try
                {
                    if (newclient.Send(m_SendData) < 0)
                    {
                        newclient.Shutdown(SocketShutdown.Both);
                        newclient.Close();
                        nConnStatus = 0;
                    }
                }
                catch (Exception e)
                {
                    Thread.Sleep(m_nSendInterval);
                    nConnStatus = 0;
                    continue;
                }
                Thread.Sleep(m_nSendInterval);
                if (sign > 0)
                {
                    m_nSendTimes--;
                }
            }
            try
            {
                if (nConnStatus == 1)
                {
                    newclient.Shutdown(SocketShutdown.Both);
                    newclient.Close();
                }
            }
            catch (System.Exception ex)
            {
                String errMsg = ex.Message;
            }
            
        }

        //停止线程
        public void Stop()
        {
            m_nRunSign = 0;
        }

        //接受线程
        public void Recv()
        {
            int len = 102400;
            byte[] buffer = new byte[len];
            int ret;
            while (true)
            {
                try
                {
                    ret = newclient.Receive(buffer);
                    if (ret <= 0)
                    {
                        break;
                    }
                }
                catch (System.Exception ex)
                {
                    String errMsg = ex.Message;
                	break;
                }
               
            }
            try
            {
                newclient.Shutdown(SocketShutdown.Both);
                newclient.Close();
            }
            catch (System.Exception ex)
            {
                String errMsg = ex.Message;
            }
        }
        private String m_IPAddr;                //对方的服务器地址
        private int m_nPort;                    //对方的端口号
        private byte[] m_SendData;              //需要发送的数据
        private int m_nSendTimes;               //发送次数
        private int m_nSendInterval;            //发送间隔
        private int m_nRunSign;                 //运行标识
        private Socket newclient;
    }
}
